package springCoreTest1;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class Test1main {

	public static void main(String[] args) {
		Resource rs = new ClassPathResource("test1Context.xml");
		BeanFactory bn = new XmlBeanFactory(rs);
		test1 emp = (test1)bn.getBean("emp");
		emp.disp();
	}
}
